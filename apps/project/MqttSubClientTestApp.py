'''
Created on Apr 19, 2019

@author: yuxuanxiong
'''
import sys
sys.path.append('/home/pi/workspace/yuxuanxiong-csye6530-python-dc3f7d8992f7/apps')
import paho.mqtt.client as mqttClient
from project.SenseHatLedActivator import SenseHatLedActivator

# create instance of client 
mcqq = mqttClient.Client()  
#create instance of SenseHatLedActivator
SenseHat = SenseHatLedActivator()                                                   # initialize the mqttClient

# The connect method and subscribe the topic 
def on_connect(clientConnect, userdata, flags, resultCode):
    print("Client connected to server. Result: " + str(resultCode))            # resultCode = 0 means connection successful
    clientConnect.subscribe('actuateTemp')                                            # subscribe topic is Test

# the publish message received and process    
def on_message(clientConnect, userdata, msg):
    payload = str(msg.payload) 
    payloadtrip = payload.lstrip("b")                                                     
    print("Received Publish message on topic {0}. Payload: {1} ".format(str(msg.topic),str(msg.payload)))   # print the topic of publish message
    print("Sensor Hat start to show!")
    SenseHat.setEnableLedFlag(True)
    # set display message
    SenseHat.setDisplayMessage(payloadtrip)
    
    


# the main thread to run the subClient
def run():
    # subscribe topic
    mcqq.on_connect = on_connect
    # mqtt message
    mcqq.on_message = on_message
    senseHat.daemon = True
    # start senseHat
    senseHat.start()
    # mqtt connect
    mcqq.connect("iot.eclipse.org", 1883, 60)  
    # loop                          
    mcqq.loop_forever()

    
if __name__ == '__main__':  
    run()