'''
Created on Apr 16, 2019

@author: yuxuanxiong
'''
import sys
sys.path.append('/home/pi/workspace/yuxuanxiong-csye6530-python-dc3f7d8992f7/apps')
from time import sleep
from project import SensorAdaptor

# create instance of SensorAdaptor
SensorAdaptor = SensorAdaptor.SensorAdaptor()
# enable emulator
SensorAdaptor.enableEmulator = True
# start sensor Adaptor
SensorAdaptor.start()
while (True):
    sleep(5)
    pass
        