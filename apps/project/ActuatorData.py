'''
Created on Apr 16, 2019

@author: yuxuanxiong
'''
import os
from datetime import datetime

# command code
COMMAND_OFF = 0
COMMAND_ON = 1
COMMAND_SET = 2
COMMAND_RESET = 3

# status code
STATUS_IDLE = 0
STATUS_ACTIVE = 1

# error code
ERROR_OK = 0
ERROR_COMMAND_FAILED = 1
ERROR_NON_RESPONSIBLE = -1

class ActuatorData():
    
    # initialize @param
    timeStamp = None
    name = 'Not set'
    hasError = False
    command = 0
    errCode = 0
    statusCode = 0
    stateData = None
    val = 0.0

    # init class
    def __init__(self):
        self.updateTimeStamp()
    
    # get command 
    def getCommand(self):
        return self.command
    
    # get name
    def getName(self):
        return self.name
    
    # get state data
    def getStateData(self):
        return self.stateData
    
    # get status code
    def getStatusCode(self):
        return self.statusCode
    
    #get error code
    def getErrorCode(self):
        return self.errCode
    
    # get value
    def getValue(self):
        return self.val;
    
    # has error
    def hasError(self):
        return self.hasError
    
    # set command
    def setCommand(self, command):
        self.command = command
    
    # set name    
    def setName(self,name):
        self.name = name
    
    # set state data    
    def setStateData(self, stateData):
        self.stateData = stateData
    
    # set status code    
    def setStatusCode(self, statusCode):
        self.statusCode = statusCode
    
    '''
    set error code
    if not equal to 0
    so has error
    '''    
    def setErrorCode(self,errCode):
        self.errCode = errCode
        
        if(self.errCode != 0):
            self.hasError = True
        else:
            self.hasError = False
    
    # set value        
    def setValue(self,val):
        self.val = val
    
    # update data    
    def updateData(self, data):
        self.command = data.getCommand()
        self.statusCode = data.getStatusCode()
        self.errCode = data.getErrorCode()
        self.stateData = data.getStateData()
        self.val = data.getValue()
    
    # update timestamp    
    def updateTimeStamp(self):
        self.timeStamp = str(datetime.now())
    
    # the format of data    
    def __str__(self):
        customStr =  \
            str(self.name  + ':' + \
                os.linesep + '\tTime:      ' + self.timeStamp + \
                os.linesep + '\tCommand:      ' + str(self.command)+ \
                os.linesep + '\tStatus Code:      ' + str(self.statusCode)+ \
                os.linesep + '\tError Code:      ' + str(self.errCode)+ \
                os.linesep + '\tState Data:      ' + str(self.stateData)+ \
                os.linesep + '\tValue:      ' + str(self.val))
            
        return customStr
                
                
                
                
        
    
   
   