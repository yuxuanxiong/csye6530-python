'''
Created on Apr 16, 2019

@author: yuxuanxiong
'''
import sys
sys.path.append('/home/pi/workspace/yuxuanxiong-csye6530-python-dc3f7d8992f7/apps')
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from project import ConfigUtil
from project import ConfigConst
import smtplib

class SmtpClientConnector:
   
    # initialize the class, constructor
    
    def __init__(self):
        self.config = ConfigUtil.ConfigUtil('../../../data/ConnectedDevicesConfig.props')
        self.config.loadConfig()
        
     
    '''
    define the publishMessage method  
    get the host, port, fromAddr, toAddr and autoToken from configConst
    package the data
    '''
      
    def publishMessage(self, topic, data):
        
        # host
        host = self.config.getProperty(ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.HOST_KEY)
        # port 
        port = self.config.getProperty(ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.PORT_KEY)
        fromAddr = self.config.getProperty(ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.FROM_ADDRESS_KEY)
        toAddr = self.config.getProperty(ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.TO_ADDRESS_KEY)
        authToken = self.config.getProperty(ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.USER_AUTH_TOKEN_KEY)
        
        msg = MIMEMultipart()
        msg['From'] = fromAddr
        msg['To'] = toAddr
        msg['Subject'] = topic
        msgBody = str(data)
        
        msg.attach(MIMEText(msgBody))
        
        msgText = msg.as_string()
        
        
        # send e-mail notification
        SMTPServer = smtplib.SMTP_SSL(host,port)
        SMTPServer.ehlo()
        SMTPServer.login(fromAddr, authToken)
        SMTPServer.sendmail(fromAddr, toAddr, msgText)
        SMTPServer.close()
        
        