'''
Created on Apr 19, 2019

@author: yuxuanxiong
'''
import sys
sys.path.append('/home/pi/workspace/yuxuanxiong-csye6530-python-dc3f7d8992f7/apps')
from project import ConfigUtil
from project import ConfigConst
from project.ActuatorData import ActuatorData

class TempActuatorEmulator:
    
    # create instance of ActuatorData
    actuatordata = ActuatorData()
    
    '''
    initialize tempActuatorEmulator
    load config from ConnectedDevicesConfig.props
    read nominalTemp from config file
    '''
    def __init__(self):
        self.config = ConfigUtil.ConfigUtil('../../data/ConnectedDevicesConfig.props')
        # load config
        self.config.loadConfig()        
        self.nominalTemp = self.config.getProperty(ConfigConst.CONSTRAINED_DEVICE, ConfigConst.NOMINAL_TEMP)
        print("The nomalTemp is = " + str(self.nominalTemp))
    
    '''
    process message
    update data and make sure the temperature is high or low
    then send message
    '''    
    def processMessage(self, data):
        self.data = data
        self.actuatordata.updateData(self.data)
        if (self.actuatordata == 1):
            msg = "warm up"
            self.actuatordata.setStateData(msg)
            
        if (self.actuatordata == 0):
            msg = "cool down"
            self.actuatordata.setStateData(msg)
            self.sensehat.setEnableLedFlag(True)
            
            message = "actuatorMessage :" +self.actuatordata.getStateData()
            self.sensehat.setDisplayMessage(message)
            
    
    # set message       
    def setMessage(self, actuatordata, command, errcode, stateData,statusCode):
        actuatordata.setCommand(command)
        actuatordata.setErrorCode(errcode)
        actuatordata.setStateData(stateData)
        actuatordata.setStatusCode(statusCode)
    
    # set check to return difference    
    def check(self, curTemp):
        
        self.curTemp = curTemp
        # calculate difference
        self.diff = self.curTemp - float(self.nominalTemp)
        diff = self.diff
        
        return diff
        
        
        
        
    
        
    