'''
Created on Apr 19, 2019

@author: yuxuanxiong
'''
from time import sleep
from sense_hat import SenseHat
import threading

class SenseHatLedActivator(threading.Thread):
    
    ''' 
    initialize @param
    @param enableLed: led enabled
    @param rotateDeg: rotate
    @param sh: null
    @param displayMsg: null 
    '''
    enableLed = False
    rateInSec = 1
    rotateDeg = 270
    sh = None
    displayMsg = None
    
    '''
     init the senseHatLedActivator 
     initial senseHat
     
     '''
    def __init__(self, rotateDeg = 270, rateInSec = 1):
        super(SenseHatLedActivator, self).__init__()
 
        if rateInSec > 0:
            self.rateInSec = rateInSec
        
        if rotateDeg >= 0:
            self.rotateDeg = rotateDeg
        
        # senseHat instance
        self.sh = SenseHat()
        # initialize senseHat
        self.sh.set_rotation(self.rotateDeg)
    
    '''
    overwrite run method
    if enabled senseHat, display message on senseHat
    then clear senseHat and sleep rateInSec time
    '''   
    def run(self):
        while True:
            if self.enableLed:
                if self.displayMsg != None:
                    # show message
                    self.sh.show_message(str(self.displayMsg))
                else:
                    self.sh.show_letter(str('R'))
                sleep(self.rateInSec)
                self.sh.clear()
            sleep(self.rateInSec)
    
    # get rateInSeconds      
    def getRateInSeconds(self):
        return self.rateInSec
    
    # set enabled flag        
    def setEnableLedFlag(self, enable):
        self.sh.clear()
        self.enableLed = enable
    
    # set display message    
    def setDisplayMessage(self, msg):
        self.displayMsg = msg
            
            