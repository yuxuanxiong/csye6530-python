'''
Created on Apr 16, 2019

@author: yuxuanxiong
'''
import sys
sys.path.append('/home/pi/workspace/yuxuanxiong-csye6530-python-dc3f7d8992f7/apps')
import threading
from time import sleep
from project.SensorData import SensorData
from project.SmtpClientConnector import SmtpClientConnector
from sense_hat import SenseHat
from project.TempActuatorEmulator import TempActuatorEmulator
from project.ActuatorData import ActuatorData
from coapthon.client.helperclient import HelperClient


#create temperature instance of DensorData
sensorData   = SensorData()
#create humidity instance of SensorData
humidity     = SensorData()
#create instance of SmtpClientConnector
connector    = SmtpClientConnector()
#create instance of SenseHat
sensehat     = SenseHat()
#create instance of TempActuatorEmulator
tempactuator = TempActuatorEmulator()
#create instance of ActuatorData
actuatordata = ActuatorData()
    
class SensorAdaptor(threading.Thread):
    '''
    initialize parameters
    @param RATE_IN_SEC: time 
    @param isPrevTempSet: compare 
    @param alertDiff: alert  
    '''
    RATE_IN_SEC   = 10
    isPrevTempSet = False
    alertDiff     = 0.1
    
    # set the initial value of ActuatorData
    actuatordata.setValue(0)
    # set the initial command code of ActuatorData
    actuatordata.setCommand(0)
    # set the initial error code of ActuatorData
    actuatordata.setErrorCode(0)
    # set the initial state data of ActuatorData
    actuatordata.setStateData(0)
    

    

    #init thread    
    def __init__(self, rateInSec = RATE_IN_SEC):
        super(SensorAdaptor, self).__init__()
        self.rateInSec = rateInSec
        
        
    '''
    overwrite run function 
    use senseHat to get temperature and humidity
    add value to sensorData
    '''       
    def run(self):
        while True:
            if self.enableEmulator:
                # senseHat get current temperature
                self.curTemp = senseHat.get_temperature()
                #senseHat get current humidity
                self.curHumi = senseHat.get_humidity()
                # add current temperature value
                sensorData.addValue(self.curTemp)
                # add current humidity value
                humidity.addValue(self.curHumi)
                
                # show the value of current temperature and humidity
                print('\n--------------------')
                print('New sensor readings:')
                print(' Temperature: ' + str(self.curTemp))
                print(' Humidity: ' + str(self.curHumi))

                ''' 
                if don't have previous temperature value
                set isPrevTempSet is true
                else send values throught COAP to gateway
                '''
                if self.isPrevTempSet == False:
                    self.prevTemp = self.curTemp
                    self.isPrevTempSet = True
                else:
                    print('\n========SENDING TEMPERATURE AND HUMIDITY TO COAP SERVER==========')
                    host = "127.0.0.1"
                    port = 5683
                    path = 'temp'
                    # connect to CoAP client
                    client = HelperClient(server=(host, port))
                    # symbol temperature value
                    self.Temp = "T" + str(self.curTemp)
                    # post value
                    response1 = client.post(path,self.Temp)
                    # print post 
                    print(response1.pretty_print())
                    #symbol humidity value
                    self.Humi = "H" + str(self.curHumi)
                    # post value
                    response2 = client.post(path,self.Humi)
                    #print post
                    print(response2.pretty_print())
                    
                    '''
                    check the difference between current temperature to previous one
                    if different from previous one, set actuatedata
                     '''
                    self.difference = tempactuator.check(self.curTemp)
                    
                    if (self.difference > 0):
                        #set message of tempactuator
                        tempactuator.setMessage(actuatordata, 0, 0, None, 1)
                        # add difference value in actuatordata
                        actuatordata.setValue(abs(difference))
                        # processMessage in tempactuator
                        tempactuator.processMessage(actuatordata)
                    
                    if (self.difference < 0):
                        #set message of tempactuator
                        tempactuator.setMessage(actuatordata, 1, 0, None, 1)
                        # add difference value in actuatordata
                        actuatordata.setValue(abs(self.difference))
                        # processMessage in tempactuator
                        tempactuator.processMessage(actuatordata)
                         
                    if (self.difference == 0):
                        print("Nothing to change")   
                    
                    #If the threshold is reached or surpassed, data should be emailed to a remote service
                    if (abs(self.curTemp - sensorData.getAvgValue()) >= self.alertDiff):
                        print('\n Current temp exceeds average by > ' + str(self.alertDiff) + '. Triggering alert...')
                    connector.publishMessage('Exceptional sensor data [test] ', sensorData)
        
            sleep(self.rateInSec)
            

