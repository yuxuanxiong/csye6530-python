'''
Created on Feb 1, 2019

@author: yuxuanxiong
'''
import threading
from time import sleep
from labs.common.SensorData import SensorData
from labs.module02.SmtpClientConnector import SmtpClientConnector
from sense_hat import SenseHat
from labs.module03.TempActuatorEmulator import TempActuatorEmulator
from labs.common.ActuatorData import ActuatorData
from random import uniform



class TempSensorAdaptor(threading.Thread):
    #init parameters
    RATE_IN_SEC   = 10
    lowVal        = 0
    highVal       = 30
    isPrevTempSet = False
    alertDiff     = 2
    
    
    #create object
    sensorData   = SensorData()
    connector    = SmtpClientConnector()
    sensehat     = SenseHat()
    tempactuator = TempActuatorEmulator()
    actuatordata = ActuatorData()
    
    actuatordata.setValue(0)
    actuatordata.setCommand(0)
    actuatordata.setErrorCode(0)
    actuatordata.setStateData(0)

    #init thread    
    def __init__(self, rateInSec = RATE_IN_SEC):
        super(TempSensorAdaptor, self).__init__()
        self.rateInSec = rateInSec
        
    #overwrite run function        
    def run(self):
        while True:
            if self.enableEmulator:
                self.curTemp = uniform(float(self.lowVal),float(self.highVal))
                self.sensorData.addValue(self.curTemp)
                
                print('\n--------------------')
                print('New sensor readings:')
                print(' ' + str(self.sensorData))
            
                if self.isPrevTempSet == False:
                    self.prevTemp = self.curTemp
                    self.isPrevTempSet = True
                else:
                    self.difference = self.tempactuator.check(self.curTemp)
                    
                    if (self.difference > 0):
                        self.tempactuator.setMessage(self.actuatordata, 0, 0, None, 1)
                        self.actuatordata.setValue(abs(self.difference))
                        self.tempactuator.processMessage(self.actuatordata)
                    
                    if (self.difference < 0):
                        self.tempactuator.setMessage(self.actuatordata, 1, 0, None, 1)
                        self.actuatordata.setValue(abs(self.difference))
                        self.tempactuator.processMessage(self.actuatordata)
                         
                    if (self.difference == 0):
                        print("Nothing to change")   
                    
                    #If the threshold is reached or surpassed, data should be emailed to a remote service
                    if (abs(self.curTemp - self.sensorData.getAvgValue()) >= self.alertDiff):
                        print('\n Current temp exceeds average by > ' + str(self.alertDiff) + '. Triggering alert...')
                    self.connector.publishMessage('Exceptional sensor data [test] ', self.sensorData)
        
            sleep(self.rateInSec)
            

