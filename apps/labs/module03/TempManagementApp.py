'''
Created on Feb 1, 2019

@author: yuxuanxiong
'''
"""
the script will simply initialize ‘tempSensorEmulator’
and start the thread, then wait indefinitely
"""

from time import sleep
from labs.module03 import TempSensorAdaptor

tempSensorAdaptor = TempSensorAdaptor.TempSensorAdaptor()
tempSensorAdaptor.enableEmulator = True
tempSensorAdaptor.start()
while (True):
    sleep(5)
    pass