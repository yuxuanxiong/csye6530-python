'''
Created on Feb 1, 2019

@author: yuxuanxiong
'''
from labs.common import ConfigUtil
from labs.common import ConfigConst
from labs.module03.SenseHatLedActivator import SenseHatLedActivator
from labs.module03.SimpleLedActivator import SimpleLedActivator
from labs.common.ActuatorData import ActuatorData

class TempActuatorEmulator:
    # create sense hat, simpleled and actuatordata instances
    sensehat     = SenseHatLedActivator()
    simpleled    = SimpleLedActivator()
    actuatordata = ActuatorData()
    
    # initialize tempActuatorEmulator, load config from ConnectedDevicesConfig.props
    # read nominalTemp from config file
    def __init__(self):
        self.config = ConfigUtil.ConfigUtil('../../../data/ConnectedDevicesConfig.props')
        self.config.loadConfig()        
        self.nominalTemp = self.config.getProperty(ConfigConst.CONSTRAINED_DEVICE, ConfigConst.NOMINAL_TEMP)
        print("The nomalTemp is = " + str(self.nominalTemp))
    
    # process message
    # update data and make sure the temperature is high or low, then send message    
    def processMessage(self, data):
        self.data = data
        self.actuatordata.updateData(self.data)
        if (self.actuatordata == 1):
            msg = "warm up"
            self.actuatordata.setStateData(msg)
            
        if (self.actuatordata == 0):
            msg = "cool down"
            self.actuatordata.setStateData(msg)
            self.sensehat.setEnableLedFlag(True)
            
            message = "actuatorMessage :" +self.actuatordata.getStateData()
            self.sensehat.setDisplayMessage(message)
            
    
            
    def setMessage(self, actuatordata, command, errcode, stateData,statusCode):
        actuatordata.setCommand(command)
        actuatordata.setErrorCode(errcode)
        actuatordata.setStateData(stateData)
        actuatordata.setStatusCode(statusCode)
        
    def check(self, curTemp):
        self.curTemp = curTemp
        self.diff = self.curTemp - float(self.nominalTemp)
        diff = self.diff
        return diff
        
        
        
        
    
        
    