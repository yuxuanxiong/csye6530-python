'''
Created on Feb 6, 2019

@author: yuxuanxiong
'''
import smbus
import threading
from time import sleep
from labs.common import ConfigUtil
from labs.common import ConfigConst
'''
@param accelAddr: accelerometer address
@param magAddr: magnetometer address
@param pressAddress: pressure address
@param humidAddr: humidity address
@param begAddr: begin address
@param totBytes: total bytes to read  
'''
i2cBus = smbus.SMBus(1) # Use I2C bus No.1 on Raspberry Pi3 +
enableControl = 0x2D
enableMeasure = 0x08
accelAddr = 0x1C
magAddr = 0x6A
pressAddr = 0x5C
humidAddr = 0x5F
begAddr = 0x28
totBytes = 6

# address for IMU (accelerometer)
# address for IMU (magnetometer)
# address for pressure sensor
# address for humidity sensor
DEFAULT_RATE_IN_SEC = 5

class I2CSenseHatAdaptor(threading.Thread):
    rateInSec = DEFAULT_RATE_IN_SEC
    
    # initialize and load config file
    def __init__(self):
        super(I2CSenseHatAdaptor, self).__init__()
        self.config = ConfigUtil.ConfigUtil(ConfigConst.DEFAULT_CONFIG_FILE_NAME)
        self.config.loadConfig()
        print('Configuration data...\n' + str(self.config))
        self.initI2CBus()
    
    # initialize  I2C
    # quick write accelerometer, magnetometer, pressure, humidity address
    # read 6 bytes data from accelAddr, magAddr, pressAddr,humidAddr seperately   
    def initI2CBus(self):
        print("Initializing I2C bus and enabling I2C addresses... ")
        
        i2cBus.write_quick(accelAddr)
        i2cBus.write_quick(magAddr)
        i2cBus.write_quick(pressAddr)
        i2cBus.write_quick(humidAddr)
        
        self.AccelerometerData = i2cBus.read_i2c_block_data(accelAddr, begAddr, totBytes)
        self.MagnetometerData = i2cBus.read_i2c_block_data(magAddr, begAddr, totBytes)
        self.PressureData = i2cBus.read_i2c_block_data(pressAddr, begAddr, totBytes)
        self.HumidityData = i2cBus.read_i2c_block_data(humidAddr, begAddr, totBytes)

    # display accelerometer block data
    # print the Accelerometer data on the console
    def displayAccelerometerData(self):
        print("Accelerometer block data: " + str(self.AccelerometerData))
    
    # display magnetometer block data 
    # print the Magnetometer data on the consle   
    def displayMagnetometerData(self):
        print("Magnetometer block data: "+ str(self.MagnetometerData))
    
    # display pressure block data
    # print the pressure data  on the console  
    def displayPressureData(self):
        print("Pressure block data: " + str(self.PressureData))
    
    # display humidity block data
    # print the humidity data on the console  
    def displayHumidityData(self):
        print("Humidity block data: " + str(self.HumidityData))
    
    # overwrite run method
    # while true, display accelerometer, magnetometer, pressure
    # and humidity data on console
    # then sleep the seconds in default    
    def run(self):
        while True:
            if self.enableEmulator:
                self.displayAccelerometerData()
                self.displayMagnetometerData()
                self.displayPressureData()
                self.displayHumidityData()
                    
            sleep(self.rateInSec)
                    