'''
Created on Feb 6, 2019

@author: yuxuanxiong
'''
from time import sleep
from labs.module04 import I2CSenseHatAdaptor

# create I2CSenseHatAdaptor instance
# start the thread
i2CSenseHatAdaptor = I2CSenseHatAdaptor.I2CSenseHatAdaptor()
i2CSenseHatAdaptor.enableEmulator = True
i2CSenseHatAdaptor.start()
while (True):
    sleep(5)
    pass