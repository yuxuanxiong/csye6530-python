'''
Created on Jan 23, 2019

@author: yuxuanxiong
'''
from time import sleep
import random
from threading import Thread 
from labs.common.SensorData import SensorData
from labs.module02.SmtpClientConnector import SmtpClientConnector

class TempSensorAdaptor(Thread):
    

    RATE_IN_SEC = 10
    
    lowVal = 0
    
    highVal = 30
    
    alertDiff = 5
    
    
    isPrevTempSet = False
    
    sensorData = SensorData()
    
    connector = SmtpClientConnector()
    
    # constructor
    # initialize the sleep time
    
    def __init__(self, rateInsec=RATE_IN_SEC):
        super(TempSensorAdaptor,self).__init__()
        self.rateInsec = rateInsec
    
    # run method
    # judge if  the enableEmulator is true or false
    # judge if the temperature dataSet is same as previous one
    # read the sensor data
    # show the data 
    
    def run(self):
        while True:
            if self.enableEmulator:
                self.curTemp = random.uniform(float(self.lowVal), float(self.highVal))
                
                self.sensorData.addValue(self.curTemp)
                
                print('\n-----------------')
                print('New sensor readings:')
                print('  ' + str(self.sensorData))
                
                if self.isPrevTempSet == False:
                    self.prevTemp = self.curTemp
                    self.isPrevTempSet = True
                else:
                    if(abs(self.curTemp - self.sensorData.getAvgValue()) >=self.alertDiff):
                        print('\n Current temp exceeds average by >' + str(self.alertDiff) + '. Triggering alert---')
                    self.connector.publishMessage('Exceptional sensor data [test]', self.sensorData)
            sleep(self.rateInsec)
        