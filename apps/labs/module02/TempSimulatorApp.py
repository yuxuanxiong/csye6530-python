'''
Created on Jan 23, 2019

@author: yuxuanxiong
'''
from time          import sleep
from labs.module02 import TempSensorAdaptor

# initialize the temperature sensor adaptor
# start the adaptor thread

tempSensorAdaptor = TempSensorAdaptor.TempSensorAdaptor()

tempSensorAdaptor.enableEmulator = True

tempSensorAdaptor.start()

while (True):
    sleep(10)
pass