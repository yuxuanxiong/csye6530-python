'''
Created on Feb 1, 2019

@author: yuxuanxiong
'''
"""
the script will simply initialize ‘tempSensorEmulator’
by creating instance named tempSensorAdaptor
and start the thread, then wait indefinitely
"""

from time import sleep
from labs.module05 import TempSensorAdaptor

tempSensorAdaptor = TempSensorAdaptor.TempSensorAdaptor()
tempSensorAdaptor.enableEmulator = True
print("Starting temp adaptor app daemon thread...")
tempSensorAdaptor.start()
while (True):
    sleep(5)
    pass