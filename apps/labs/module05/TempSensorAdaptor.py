'''
Created on Feb 1, 2019

@author: yuxuanxiong
'''
import threading
from time import sleep
from labs.common.SensorData import SensorData
from labs.module02.SmtpClientConnector import SmtpClientConnector
from sense_hat import SenseHat
from labs.module03.TempActuatorEmulator import TempActuatorEmulator
from labs.common.ActuatorData import ActuatorData
from random import uniform
from labs.common.DataUtil import DataUtil



class TempSensorAdaptor(threading.Thread):
    #init parameters
    RATE_IN_SEC   = 10
    lowVal        = 0
    highVal       = 30
    isPrevTempSet = False
    alertDiff     = 2
    
    
    #create objects of SensorData, SmtpClientConnector, SenseHat, TempActuatorEmulator, ActuatorData, DataUtil
    sensorData   = SensorData()
    connector    = SmtpClientConnector()
    sensehat     = SenseHat()
    tempactuator = TempActuatorEmulator()
    actuatordata = ActuatorData()
    dataUtil = DataUtil()
    
    actuatordata.setValue(0)
    actuatordata.setCommand(0)
    actuatordata.setErrorCode(0)
    actuatordata.setStateData(0)

    #init thread 
    # set rateInSec   
    def __init__(self, rateInSec = RATE_IN_SEC):
        super(TempSensorAdaptor, self).__init__()
        self.rateInSec = rateInSec
        
    '''
    overwrite run method
    if temperature is not same as previous, then change prevTemp = curTemp
    check the difference between current temperature and nominal temperature
    if the current temperature is higher or lower than nominal temperature
    then send difference and messages to actuator
    if current temperature minus average value is exceed to alert 
    the application will send sensor data email
    @param param: difference: the difference between current temp and nominal temp
    '''        
    def run(self):
        while True:
            if self.enableEmulator:
                self.curTemp = uniform(float(self.lowVal),float(self.highVal))
                self.sensorData.addValue(self.curTemp)
                
                print('\n--------------------')
                print('New sensor readings:')
                print(' ' + str(self.sensorData))
            
                if self.isPrevTempSet == False:
                    self.prevTemp = self.curTemp
                    self.isPrevTempSet = True
                else:
                    self.difference = self.tempactuator.check(self.curTemp)
                    
                    if (self.difference > 0):
                        self.tempactuator.setMessage(self.actuatordata, 0, 0, None, 1)
                        self.actuatordata.setValue(abs(self.difference))
                        self.tempactuator.processMessage(self.actuatordata)
                    
                    if (self.difference < 0):
                        self.tempactuator.setMessage(self.actuatordata, 1, 0, None, 1)
                        self.actuatordata.setValue(abs(self.difference))
                        self.tempactuator.processMessage(self.actuatordata)
                         
                    if (self.difference == 0):
                        print("Nothing to change")   
                    
                    #If the threshold is reached or surpassed, data should be emailed to a remote service
                    if (abs(self.curTemp - self.sensorData.getAvgValue()) >= self.alertDiff):
                        print('\n Current temp exceeds average by > ' + str(self.alertDiff) + '. Triggering alert...')
                    self.connector.publishMessage('Exceptional sensor data [test]', self.dataUtil.toJson(self.sensorData.name,self.sensorData.timeStamp, self.sensorData.avgValue,self.sensorData.minValue, self.sensorData.maxValue,self.sensorData.curValue, self.sensorData.totValue, self.sensorData.sampleCount))
        
            sleep(self.rateInSec)
            

