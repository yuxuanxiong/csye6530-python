'''
Created on Mar 17, 2019

@author: yuxuanxiong
'''

from coapthon.client.helperclient import HelperClient
from labs.common.SensorData import SensorData
from labs.common.DataUtil import DataUtil

# initialize the host, post and path
host = "127.0.0.1"
port = 5683
path = 'temp'

# initialize sensorData and DataUtil
sd = SensorData()
du = DataUtil()
sdJson = du.toJson(sd.name,sd.timeStamp,sd.avgValue,sd.minValue,sd.maxValue,sd.curValue,sd.totValue,sd.sampleCount) # transfer SensorData instance into JSON format

print("The JSON format SensorData of client is: ")      # print JSON format sensorData
print(sdJson)

client = HelperClient(server=(host, port))              # if connect the server success, print ping successful
print("Ping Successful!")

response = client.get(path)                             # the method of GET request
print("Testing GET method:")
print(response.pretty_print())                          # print the response from server

print("Testing POST method:")                           # the method of POST request
response = client.post(path, sdJson)
print(response.pretty_print())

print("Testing PUT method:")                            # the method of PUT request
response = client.put(path, sdJson)
print(response.pretty_print())

print("Testing DELETE method:")                         # the method of DELETE request
response = client.delete(path)
print(response.pretty_print())                          
client.stop()