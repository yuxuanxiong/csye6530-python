'''
Created on Feb 28, 2019

@author: yuxuanxiong
'''

import paho.mqtt.client as mqttClient
from labs.common.DataUtil import DataUtil

mcqq = mqttClient.Client()                                                     # initialize the mqttClient

# The connect method and subscribe the topic 
def on_connect(clientConnect, userdata, flags, resultCode):
    print("Client connected to server. Result: " + str(resultCode))            # resultCode = 0 means connection successful
    clientConnect.subscribe('Test')                                            # subscribe topic is Test

# the publish message received and process    
def on_message(clientConnect, userdata, msg):
    payload = str(msg.payload)                                                      
    print("Received Publish message on topic {0}. ".format(str(msg.topic)))   # print the topic of publish message
    if not payload:
        return
    payloadtrip = payload.lstrip("b")
    payloadtriptrip = payloadtrip.lstrip("'").rstrip("'")                    # delete extra signs of message
    print("Received message:")
    print(payloadtriptrip)
    dataUtil = DataUtil();
    du=dataUtil.jsonToSensorData(payloadtriptrip)                           # transfer JSON into sensorData instance
    print("Transfer Json to SensorData instance:")
    print(du) 
    print("Transfer SensorData instance to JSon:")
    dataUtil.toJson(du.name, du.timeStamp, du.avgValue, du.minValue, du.maxValue, du.curValue, du.totValue, du.sampleCount)  # transfer SensorData instance to JSON
    mcqq.unsubscribe(msg.topic)                                             # unsubscribe from the topic


# the main thread to run the subClient
def run():
    mcqq.on_connect = on_connect
    mcqq.on_message = on_message
    mcqq.connect("iot.eclipse.org", 1883, 60)                            
    mcqq.loop_forever()

    
if __name__ == '__main__':  
    run()