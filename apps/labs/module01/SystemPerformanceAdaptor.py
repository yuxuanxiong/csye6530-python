'''
Created on Jan 17, 2019

@author: yuxuanxiong
'''
import psutil
from time import sleep
from threading import Thread 
class SystemPerformanceAdaptor(Thread):
    
    # set the default sleep time is 10 seconds
    
    RATE_IN_SEC=10
    
    # constructor of SystemPerformanceAdaptor class
    # input the sleep time
    
    def __init__(self, rateInSec=RATE_IN_SEC):
        super(SystemPerformanceAdaptor,self).__init__()
        self.rateInSec = rateInSec
    
    # run method to print the system performance by using psutil

    def run(self):
        while True:
            if self.enableAdaptor:
                print('\n--------------------')
                print('New system performance readings:')
                print('  ' + str(psutil.cpu_stats()))
                print('  ' + str(psutil.virtual_memory()))
            
            sleep(self.rateInSec)