'''
Created on Jan 26, 2019

@author: yuxuanxiong
'''
import configparser
import os

class ConfigUtil:
    '''
    set the config path
    set the data is not being loaded at the beginning
    '''
    configPath = '../../../data'
    configFile = configPath + '/' + 'ConnectedDevicesConfig.props'
    configData = configparser.ConfigParser()
    isLoaded   = False
    
    '''
    Constructor for ConfigUtil.
    
    @param configFile The name of the configuration file to load.
    '''
    def __init__(self, configFile = None):
        if (configFile != None):
            self.configFile = configFile
    
    '''
    Attempts to load the config file using the name passed into
    the constructor.
     
    '''
    def loadConfig(self):
        print(str(os.listdir(self.configPath)))
        
        if (os.path.exists(self.configPath)):
            print("Loading config: " + self.configFile)
            self.configData.read(self.configFile)
            self.isLoaded = True
        else:
            print("Failed to OS-check config. Will try to load: " + self.configFile)
            self.configData.read(self.configFile)
            self.isLoaded = True
        
        #print("Config: " + str(self.configData.sections()))

    '''
    Returns the entire configuration object. If the config file hasn't
    yet been loaded, it will be loaded.
    
    @param forceReload Defaults to false; if true, will reload the config.
    @return: The entire configuration file.
     
    '''
    def getConfig(self, forceReload = False):
        if (self.isLoaded == False or forceReload):
            self.loadConfig()
        
        return self.configData
    
    '''
    Returns the name of the configuration file.
    
    @return: The name of the config file.
    '''
    def getConfigFile(self):
        return self.configFile

    '''
    Attempts to retrieve the value of 'key' from the config.
    
    @param: section The name of the section to parse.
    @param: key The name of the key to lookup in 'section'.
    @param: forceReload Defaults to false; if true will reload the config.
    @return: The property associated with 'key' in 'section'.
    '''
    def getProperty(self, section, key, forceReload = False):
        return self.getConfig(forceReload).get(section, key)
    
    '''
    Simple boolean check if the config data is loaded or not.
    
    @return: boolean True on success; false otherwise.
    '''
    def isConfigDataLoaded(self):
        return self.isLoaded