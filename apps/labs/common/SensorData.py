'''
Created on Jan 23, 2019

@author: yuxuanxiong
'''
import os

from datetime import datetime


class SensorData():
    # initialize the timeStamp, name, curValue, avgValue, minValue,
    # maxValue, totValue and sampleCount
    timeStamp =  None
    name      =  'Temprature'
    curValue = 0
    avgValue = 0
    minValue = 30
    maxValue = 0
    totValue = 0
    sampleCount = 0
    
    
    # constructor of SensorData class
    # set the timeStamp is equal to the datetime now
    def __init__(self):
        self.timeStamp = str(datetime.now())
    
    '''
    This function is to addValue from newVal
    add sampleCount
    refresh the time
    set current value is equal to new value
    add the new value to total value
    
    if current value is smaller than minimum value, then minValue = curValue
    if current value is larger than maximum value, then maxValue = curValue
    if current value is not zero and samplecount is larger than 0, then calculate average of temperature
    @param param: newVal: the new value that sensorData genetated
    '''   
    def addValue(self,newVal):
        self.sampleCount +=1
        self.timeStamp = str(datetime.now())
        self.curValue = newVal
        self.totValue += newVal
        
        if(self.curValue < self.minValue):
            self.minValue = self.curValue
            
        if(self.curValue > self.maxValue):
            self.maxValue = self.curValue
        
        if(self.curValue != 0 and self.sampleCount > 0):
            self.avgValue = self.totValue  / self.sampleCount
    
    # get the average value        
    def getAvgValue(self):
        return self.avgValue
    
    #get max value
    def getMaxValue(self):
        return self.maxValue
    
    #get minimum value
    def getMinValue(self):
        return self.minValue
    
    #get current value
    def getValue(self):
        return self.curValue
    
    #set the name
    def setName(self, name):
        self.name=name
    
    # set the output format of all data    
    def __str__(self):
        customStr = \
        str(self.name + ':' + \
            os.linesep + '\tTime:    ' + self.timeStamp + \
            os.linesep + '\tCurrent:    ' + str(self.curValue) + \
            os.linesep + '\tAverage:   ' +str( self.avgValue)   +  \
            os.linesep + '\tSamples:   '  + str(self.sampleCount) + \
            os.linesep + '\tMin:   ' + str(self.minValue) + \
            os.linesep + '\tMax:    ' + str(self.maxValue))
        
        return customStr
    