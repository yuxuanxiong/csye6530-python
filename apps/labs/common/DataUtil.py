'''
Created on Feb 17, 2019

@author: yuxuanxiong
'''
import json
from labs.common import ActuatorData
from labs.common.SensorData import SensorData

class DataUtil:
 
    '''
    toJson function is to convert data to JSon string
    and write it in a json file
    @param param: name: name from SensorData
    @param param: timeStamp: the time of data generated 
    @param param: avgValue: average temperature from SensorData
    @param param: maxValue: the maximum temperature that SensorData generated
    @param param: minValue: the minimum temperature that SesorData generated
    @param param: totValue: the total temperature that SensorData generated
    @param param: sampleCount: the number of sample generated
    @param param: json_info: the data from sensorData
    @return: JSon string
    '''
 
    def toJson(self, name,timeStamp,avgValue,minValue,maxValue,curValue,totValue,sampleCount):
        
        json_info = {}
        json_info['name'] = name
        json_info['timeStamp'] = timeStamp
        json_info['avgValue'] = avgValue
        json_info['minValue'] = minValue
        json_info['maxValue'] = maxValue
        json_info['curValue'] = curValue
        json_info['totValue'] = totValue
        json_info['sampleCount'] = sampleCount
        
        with open("/Users/yuxuanxiong/git/iot-gateway-python/data/data.json","w+") as j:
            
            print("Dictionary:  ")
            print(json_info)
            json.dump(json_info,j)
            
            jsonData = json.dumps(json_info)
            # write data into json file

            print("JSon:  ")
            print(jsonData)
        
        return jsonData
        
        
    '''
    This function is to convert JSon string to ActuatorData instances
    then return ActuatorData instance
    @param name: sensor name
    @param timeStamp: time of data generated
    @param  hasError: false means no error, true means has error
    @param param: command: COMMAND_OFF = 0, COMMAND_ON = 1, COMMAND_SET = 2, COMMAND_RESET = 3
    @param param: errCode: ERROR_OK = 0, ERROR_COMMAND_FAILED = 1, ERROR_NON_RESPONSIBLE = -1
    @param param: statusCode: idle = 0 , active = 1
    @param param: stateData: None
    @param param: curValue: current temperature
    
    '''       
    def jsonToActuatorData(self, jsonData):
        adDict = json.loads(jsonData)
        
        print(" decode [pre] --> " + str(adDict))
        
        ad = ActuatorData.ActuatorData()
        ad.name = adDict['name']
        ad.timeStamp = adDict['timeStamp']
        ad.hasError = adDict['hasError']
        ad.command = adDict['command']
        ad.errCode = adDict['errCode']
        ad.statusCode = adDict['statusCode']
        ad.stateData = adDict['stateData']
        ad.curValue = adDict['curValue']
        
        print(" decode [post] --> " + str(ad))
        
        return ad
        
        
        
        
    '''
    This function is to convert json string to SensorData instances
    create sensorData object of sd
    then set attributes from json string
    return sd
    '''   
    def jsonToSensorData(self,jsonData):
        
        sdDict = json.loads(jsonData)
        
        sd = SensorData()
        sd.name = sdDict['name']
        sd.timeStamp = sdDict['timeStamp']
        sd.avgValue = sdDict['avgValue']
        sd.minValue = sdDict['minValue']
        sd.maxValue = sdDict['maxValue']
        sd.curValue = sdDict['curValue']
        sd.totValue = sdDict['totValue']
        sd.sampleCount = sdDict['sampleCount']

        return sd